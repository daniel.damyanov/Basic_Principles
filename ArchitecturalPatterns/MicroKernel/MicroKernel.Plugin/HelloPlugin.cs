﻿using System;
using Microkernel.Contract;

namespace MicrokernelHandsOn.Plugin
{
    public class HelloPlugin : IPlugin  
    {
        public void SaySomething()
        {
            Console.WriteLine("Hello Microkernel");
        }
    }
}
