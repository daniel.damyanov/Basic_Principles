﻿using System;
using Microkernel.Contract;

namespace MicrokernelHandsOn.Plugin
{
    public class GoodByPlugin : IPlugin
    {
        public void SaySomething()
        {
            Console.WriteLine("Goodbye Microkernel");
        }
    }
}