﻿using CommandLibrary.ShoppingCartExample;
using System;

namespace Command_ShoppingCart
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ShoppingCartExecutor.Execute();
            
        }
    }
}
