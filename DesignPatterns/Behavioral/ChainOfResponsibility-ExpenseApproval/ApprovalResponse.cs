﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility_ExpenseApproval
{
    public enum ApprovalResponse
    {
        Denied,
        Approved,
        BeyondApprovalLimit
    }
}
