﻿using ChainOfResponsibilityLibrary.ApprovalExample;
using DesignPatternsLibrary.PatternExecutors;
using System;

namespace ChainsOfResponsibility_Purchase
{
    internal class Program
    {
        static void Main(string[] args)
        {
           Executor e=new Executor();
            e.Execute();
        }
    }
    public class Executor : PatternExecutor
    {
        public override string Name => "Chain Of Responsibility - Behavioral Pattern";

        public override void Execute()
        {
            PurchaseApprovalExecutor.Execute();
        }
    }
}
