﻿using DesignPatternsLibrary.PatternExecutors;
using IteratorLibrary.MusicFestivalsExample;

namespace IteratorLibrary
{
    public class Executor : PatternExecutor
    {
        public override string Name => "Iterator - Behavioral Pattern";

        public override void Execute()
        {
            MusicFestivalsExecutor.Execute();
        }
    }
}
