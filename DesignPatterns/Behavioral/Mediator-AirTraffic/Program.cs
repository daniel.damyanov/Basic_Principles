﻿using MediatorLibrary;
using MediatorLibrary.AirTrafficControlExample.Components;
using MediatorLibrary.AirTrafficControlExample.Components.Common;
using MediatorLibrary.AirTrafficControlExample.Mediators;
using System;

namespace Mediator_AirTraffic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            SofiaAirTrafficControl sofiaAirTraffic= new SofiaAirTrafficControl();  
           Aircraft aircraft= new Boeing737("111",200000,sofiaAirTraffic);
            sofiaAirTraffic.ReceiveAircraftLocation(aircraft);
            Executor e=new Executor();
            e.Execute();
        }
    }
}
