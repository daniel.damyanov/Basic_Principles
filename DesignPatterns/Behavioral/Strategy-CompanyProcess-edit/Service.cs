﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_CompanyProcess_edit
{
    public class Service : IMessageService
    {
        public void WorkProcess(string message)
        {
            Console.WriteLine($"Process Service! :{message}");
        }
    }
}
