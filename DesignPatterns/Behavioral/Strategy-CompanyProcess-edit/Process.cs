﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_CompanyProcess_edit
{
    public class Process
    {
        IMessageService _bussines = null;
        public Process(IMessageService bussines)
        {
            _bussines = bussines;
        }

        public void WorkProcess(string message)
        {
            _bussines.WorkProcess(message);
        }
    }
}
