﻿using DesignPatternsLibrary.PatternExecutors;
using MementoLibrary.FoodSupplierExample;

namespace MementoLibrary
{
    public class Executor : PatternExecutor
    {
        public override string Name => "Memento - Behavioral Pattern";

        public override void Execute()
        {
            FoodSupplierExecutor.Execute();
        }
    }
}
