﻿using System;

namespace Visitor
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Setup employee collection
            var employees = new Employees();
            employees.Attach(new Clerk());
            employees.Attach(new Director());
            employees.Attach(new President());

            // Employees are 'visited'
            employees.Accept(new IncomeVisitor());
            employees.Accept(new VacationVisitor());
        }
    }
}
