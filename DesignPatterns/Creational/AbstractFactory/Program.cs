﻿using AbstractFactory.PizzaPlaces;
using System;

namespace AbstractFactory
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var pizzaPlace = new PizzaExtraordinaire();
            var yamYam = new OnlineDeliveryCompany(pizzaPlace);

            var cheesePizza = yamYam.DeliverCheesePizza();

            Console.WriteLine(cheesePizza.Name);
            Console.WriteLine("Ingridients: ");
            foreach (var ingridient in cheesePizza.Ingridients)
            {
                Console.WriteLine(ingridient);
            }
        }
    }
}
