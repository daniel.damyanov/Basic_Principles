﻿namespace FactoryMethod.GsmConglomerate
{
    public class GalaxyComputers : Manufacturer
    {
        public override Gsm ManufactureGsm()
        {
            var phone = new GalaxyStar
                                    {
                                        BatteryLife = 999,
                                        Height = 199,
                                        Weight = 99,
                                        Width = 49
                                    };

            return phone;
        }
    }
}
