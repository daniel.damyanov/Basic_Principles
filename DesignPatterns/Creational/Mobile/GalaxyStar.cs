﻿namespace FactoryMethod.GsmConglomerate
{
    using System;

    public class GalaxyStar : Gsm
    {
        public GalaxyStar()
        {
            this.Name = "Samun Galaxy";
        }

        public override void Start()
        {
            Console.WriteLine("Starting up the Galaxy...");
            Console.WriteLine("Thrusters on full!");
        }
    }
}
