﻿using FactoryMethod.GsmConglomerate;
using System;

namespace Mobile
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var pearComp = new PearComputers();
            var samunComp = new GalaxyComputers();

            Gsm firstPhone = pearComp.ManufactureGsm();
            Gsm secondPhone = samunComp.ManufactureGsm();

            PrintGsmInfo(firstPhone);
            firstPhone.Start();

            Console.WriteLine();

            PrintGsmInfo(secondPhone);
            secondPhone.Start();
        }

        public static void PrintGsmInfo(Gsm gsm)
        {
            Console.WriteLine("Phone name: {0}", gsm.Name);
            Console.WriteLine("Height: {0}", gsm.Height);
            Console.WriteLine("Width: {0}", gsm.Width);
            Console.WriteLine("Weight: {0}", gsm.Weight);
            Console.WriteLine("Battery life: {0}", gsm.BatteryLife);
        }
    }
}
