﻿using Builder;
using System;

namespace VehicleShop
{
    internal class Program
    {
        static void Main(string[] args)
        {
            VehicleBuilder builder;
            var shop = new Shop();

            builder = new ScooterBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            builder = new CarBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();

            builder = new MotorCycleBuilder();
            shop.Construct(builder);
            builder.Vehicle.Show();
        }
    }
}
