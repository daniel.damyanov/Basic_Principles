﻿using DesignPatternsLibrary.PatternExecutors;
using PrototypeLibrary.WebPageScraperExample;

namespace PrototypeLibrary
{
    public class Executor : PatternExecutor
    {
        public override string Name => "Prototype - Creational Pattern";

        public override void Execute()
        {
            WebPageScraperExecutor.Execute();
        }
    }
}
