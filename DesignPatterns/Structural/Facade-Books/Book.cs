﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade_Books
{
    public class Book
    {
        public Book(string isbn)
        {
            Isbn = isbn;
        }
        public string Author { get; set; }

        public string Title { get; set; }

        public string Publisher { get; set; }

        public string Isbn { get; set; }
    }
}
