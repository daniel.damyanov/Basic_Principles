﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter_Employee
{
    public interface ITarget
    {
        IEnumerable<string> GetEmployeeList();
    }
}
