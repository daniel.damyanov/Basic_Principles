﻿using System;

namespace Adapter_Employee
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ITarget target = new EmployeeAdapter();

            var client = new FancyReportingTool(target);

            client.ShowEmployeeList();

            Console.ReadKey();
        }
    }
}
