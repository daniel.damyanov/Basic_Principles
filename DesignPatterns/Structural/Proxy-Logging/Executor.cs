﻿using DesignPatternsLibrary.PatternExecutors;
using ProxyLibrary.LoggingProxyExample;

namespace ProxyLibrary
{
    public class Executor : PatternExecutor
    {
        public override string Name => "Proxy - Structural Pattern";

        public override void Execute()
        {
            LoggingProxyExecutor.Execute();
        }
    }
}
