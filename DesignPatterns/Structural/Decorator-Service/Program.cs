﻿using Microsoft.Extensions.Caching.Memory;
using System;

namespace Decorator_Service
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IMemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            IDataService service = new DataService();
            DataServiceCachingDecorator dataServiceCaching = new DataServiceCachingDecorator(service, cache);

            dataServiceCaching.GetData().ForEach(x => Console.WriteLine(x));
            
        }
    }
}
