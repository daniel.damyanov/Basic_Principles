﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Decorator_Service
{
    public class DataService : IDataService
    {
        public List<int> GetData()
        {
            var data = new List<int>();

            for (var i = 0; i < 10; i++)
            {
                data.Add(i);

                Thread.Sleep(350);
            }

            return data;
        }
    }
}
