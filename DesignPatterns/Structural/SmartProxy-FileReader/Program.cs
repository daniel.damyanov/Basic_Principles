﻿using System;

namespace SmartProxy_FileReader
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IFileWriter fileWriter = new FileWriter();
            fileWriter.WriteTwiceToSameFile("file.txt", "This is from File Writer");
        }
    }
}
