﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartProxy_FileReader
{
    public interface IFileWriter
    {
        void WriteTwiceToSameFile(string outputFile, string message);
    }
}
