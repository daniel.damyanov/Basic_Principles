﻿using DecoratorLibrary.PancakeExample;
using DesignPatternsLibrary.PatternExecutors;

namespace DecoratorLibrary
{
    public class Executor : PatternExecutor
    {
        public override string Name => "Decorator - Structural Pattern";

        public override void Execute()
        {
            PancakeExecutor.Execute();
        }
    }
}
