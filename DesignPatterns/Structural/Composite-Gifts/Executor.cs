﻿using CompositeLibrary.GiftExample;
using DesignPatternsLibrary.PatternExecutors;

namespace CompositeLibrary
{
    public class Executor : PatternExecutor
    {
        public override string Name => "Composite - Structural Pattern";

        public override void Execute()
        {
            GiftExecutor.Execute();
        }
    }
}
