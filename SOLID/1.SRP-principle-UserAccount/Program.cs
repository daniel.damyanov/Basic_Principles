﻿using System;

namespace _1.SRP_principle_UserAccount
{
    internal class Program
    {
        public static void Main()
        {
            Console.WriteLine("Welcome to the application");
            Person user = new Person();
            Console.WriteLine("Enter your first name: ");
            user.FirstName = Console.ReadLine();

            Console.WriteLine("Enter your last name: ");
            user.LastName = Console.ReadLine();

            if (string.IsNullOrEmpty(user.FirstName))
            {
                Console.WriteLine("Not a valid first name");
                Console.ReadLine();
                return;
            }
            if (string.IsNullOrEmpty(user.FirstName))
            {
                Console.WriteLine("Not a valid last name");
                Console.ReadLine();
                return;
            }
            Console.WriteLine($"Your name is {user.FirstName} {user.LastName}");
            Console.ReadLine();
        }

    }
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
