﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.LS_principle_Edit
{
    public class DetailsPrinter
    {
        private readonly IEnumerable<Employee> employees;

        public DetailsPrinter(IEnumerable<Employee> employees)
        {
            this.employees = employees;
        }

        public void Print()
        {
            foreach (var employee in this.employees)
            {
                var details = employee.ToString();

            }
        }
    }
}
