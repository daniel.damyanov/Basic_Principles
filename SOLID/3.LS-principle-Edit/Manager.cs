﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.LS_principle_Edit
{
    public class Manager : Employee
    {
        public IEnumerable<string> Documents { get; set; }

        public override string ToString()
        {
            return base.ToString() + "; Documents: " + this.Documents;
        }
    }
}
