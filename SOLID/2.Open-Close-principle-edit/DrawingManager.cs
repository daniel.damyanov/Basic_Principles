﻿using _2.Open_Close_principle_edit.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Open_Close_principle_edit
{
    internal abstract class DrawingManager : IDrawingManager
    {
        public abstract void Draw(IShape shape);
    }
}
