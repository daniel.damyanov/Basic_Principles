﻿namespace _4.IS_principle_Worker
{
    using _4.IS_principle_Worker.Contracts;

    public class Human : IWorker
    {
        public void Eat()
        {
            // eat
        }

        public void Work()
        {
            // work
        }

        public void Sleep()
        {
            // sleep
        }
    }
}
