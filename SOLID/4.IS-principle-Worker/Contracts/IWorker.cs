﻿namespace _4.IS_principle_Worker.Contracts
{
    public interface IWorker
    {
        void Eat();

        void Work();

        void Sleep();
    }
}
