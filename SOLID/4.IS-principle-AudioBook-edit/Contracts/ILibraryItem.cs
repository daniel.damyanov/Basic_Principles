﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.IS_principle_AudioBook_edit.Contracts
{
    public interface ILibraryItem
    {
        public string LibraryId { get; set; }
        public string Title { get; set; }
    }
}
