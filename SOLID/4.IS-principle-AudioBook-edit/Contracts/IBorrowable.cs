﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.IS_principle_AudioBook_edit.Contracts
{
    public interface IBorrowable
    {
        int CheckOutDurationInDays { get; set; }
        string Borrower { get; set; }
        DateTime BorrowDate { get; set; }

        void CheckOut(string borrower);

        void CheckIn();

        DateTime GetDueDate();
    }
}
