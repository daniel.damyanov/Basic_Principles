﻿using _4.IS_principle_AudioBook_edit.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.IS_principle_AudioBook_edit.AudioBook
{
    public interface IAudioBook:ILibraryItem
    {
        public int RuntimeInMinutes { get; set; }
    }
}
