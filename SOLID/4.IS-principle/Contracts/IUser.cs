﻿namespace _4.IS_principle
{
    public interface IUser
    {
        string Email { get; }

        string PasswordHash { get; }
    }
}
