﻿namespace _4.IS_principle
{
    using _4.IS_principle;

    public class AccountContoller
    {
        private readonly IAccount manager;

        public AccountContoller(IAccount manager)
        {
            this.manager = manager;
        }

        public void ChangePassword(string oldPass, string newPass)
        {
            this.manager.ChangePassword(oldPass, newPass);
        }
    }
}
