﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.SRP_principle_Edit.Contracts
{
    internal interface IShape
    {
        public decimal Area { get; }
    }
}
