﻿using _1.SRP_principle_UserAccount_Edit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.SRP_principle_UserAccount_Edit.Core
{
    internal class PersonValidator
    {
        public static bool Validate(Person person)
        {
            if (string.IsNullOrWhiteSpace(person.FirstName))
            {
                Messages.DisplayValidationError("first name");
                return false;
            }
            if (string.IsNullOrWhiteSpace(person.LastName))
            {
                Messages.DisplayValidationError("last name");
                return false;
            }
            return true;
        }
    }
}
