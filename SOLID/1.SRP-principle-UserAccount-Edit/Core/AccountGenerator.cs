﻿using _1.SRP_principle_UserAccount_Edit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.SRP_principle_UserAccount_Edit.Core
{
    internal class AccountGenerator
    {
        public static void CreateAccount(Person person)
        {
            Console.WriteLine($"Hello user {person.FirstName.Substring(0, 1)}.{person.LastName}");
        }
    }
}
