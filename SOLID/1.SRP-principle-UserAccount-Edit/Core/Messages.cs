﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.SRP_principle_UserAccount_Edit.Core
{
    internal class Messages
    {
        public static void WelcomeMessage()
        {
            Console.WriteLine("Hello user!");
        }
        public static void EndApplication()
        {
            Console.WriteLine("Press enter to close!");
            Console.ReadLine();
        }
        public static void DisplayValidationError(string fieldName)
        {
            Console.WriteLine($"You did not enter a valid  {fieldName}");
        }
    }
}
