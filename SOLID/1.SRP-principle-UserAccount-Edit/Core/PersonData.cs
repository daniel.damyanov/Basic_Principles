﻿using _1.SRP_principle_UserAccount_Edit.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.SRP_principle_UserAccount_Edit.Core
{
    internal class PersonData
    {
        public static Person CreateUser()
        {
            Person person = new Person();

            Console.WriteLine("Enter your first name");
            person.FirstName = Console.ReadLine();

            Console.WriteLine("Enter your last name");
            person.LastName = Console.ReadLine();

            return person;
        }
       
    }
}
