﻿using _1.SRP_principle_UserAccount_Edit.Core;
using _1.SRP_principle_UserAccount_Edit.Models;
using System;

namespace _1.SRP_principle_UserAccount_Edit
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Messages.WelcomeMessage();
            Person user = PersonData.CreateUser();
            bool isUserValid = PersonValidator.Validate(user);
            if (isUserValid == false)
            {
                Messages.EndApplication();
                return;
            }
            AccountGenerator.CreateAccount(user);
            Messages.EndApplication();
        }
    }
}
