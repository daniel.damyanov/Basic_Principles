﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Open_Close_principle.Contracts
{
    internal interface IDrawingManager
    {
        void Draw(IShape shape);
    }
}
