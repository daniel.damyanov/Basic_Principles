﻿using _2.Open_Close_principle.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Open_Close_principle
{
    internal class DrawingManager : IDrawingManager
    {
        public void Draw(IShape shape)
        {
            if (shape is Circle)
            {
                this.DrawCircle(shape as Circle);
            }
            else if (shape is Rectangle)
            {
                this.DrawRectangle(shape as Rectangle);
            }
        }

        private void DrawRectangle(Rectangle rectangle)
        {
            // Draw Rectangle
        }

        private void DrawCircle(Circle circle)
        {
            // Draw Circle
        }
    }
}
