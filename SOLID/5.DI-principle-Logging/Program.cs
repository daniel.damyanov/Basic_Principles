﻿using DemoLibrary;
using System;

namespace DIprinciple
{
    class Program
    {
        static void Main(string[] args)
        {
            IPerson owner = Factory.CreatePerson();

            owner.FirstName = "Gosho";
            owner.LastName = "Peshev";
            owner.EmailAddress = "g.peshev@demo.com";
            owner.PhoneNumber = "08888888";


            IServant servant = Factory.CreateServant();

            servant.ServantName = "Take out the trash";
            servant.Owner = owner;
           

            servant.PerformedWork(3);
            servant.PerformedWork(1.5);
            servant.CompleteServant();

            Console.ReadLine();
        }
    }
}
