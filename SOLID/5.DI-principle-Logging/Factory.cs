﻿using DemoLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DIprinciple
{
    public static class Factory
    {
        public static IEmailer CreateEmailer()
        {
            return new Emailer();
        }
        public static ILogger CreateLogger()
        {
            return new Logger();
        }
        public static IPerson CreatePerson()
        {
            return new Person();
        }
        public static IServant CreateServant()
        {
            return new Servant(CreateLogger(),CreateEmailer());
        }
    }
}
