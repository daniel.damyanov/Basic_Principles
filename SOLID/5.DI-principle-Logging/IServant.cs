﻿namespace DemoLibrary
{
    public interface IServant
    {
        double HoursWorked { get; }
        bool IsComplete { get; }
        IPerson Owner { get; set; }
        string ServantName { get; set; }

        void CompleteServant();
        void PerformedWork(double hours);
    }
}