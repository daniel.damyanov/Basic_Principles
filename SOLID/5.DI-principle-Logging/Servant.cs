﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibrary
{
    public class Servant : IServant
    {
        public string ServantName { get; set; }
        public IPerson Owner { get; set; }
        public double HoursWorked { get; private set; }
        public bool IsComplete { get; private set; }
        private ILogger Logger { get; set; }
        private IEmailer Emailer { get; set; }
        public Servant(ILogger logger, IEmailer emailer)
        {
            Logger = logger;
            Emailer = emailer;
        }
        public void PerformedWork(double hours)
        {
            HoursWorked += hours;
            Logger.Log($"Performed work on { ServantName }");
        }

        public void CompleteServant()
        {
            IsComplete = true;
            Logger.Log($"Completed { ServantName }");
            Emailer.SendEmail(Owner, $"The servant { ServantName } is complete.");
        }
    }
}
