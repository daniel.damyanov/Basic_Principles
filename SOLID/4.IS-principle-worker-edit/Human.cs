﻿namespace _4.IS_principle_worker_edit
{
    using _4.IS_principle_worker_edit.Contracts;

    public class Human : IWorker, ISleeper, IEater
    {
        public void Eat()
        {
            // eat
        }

        public void Work()
        {
            // work
        }

        public void Sleep()
        {
            // sleep
        }
    }
}
