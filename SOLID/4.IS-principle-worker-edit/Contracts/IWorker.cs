﻿namespace _4.IS_principle_worker_edit.Contracts
{
    public interface IWorker
    {
        void Work();
    }
}
