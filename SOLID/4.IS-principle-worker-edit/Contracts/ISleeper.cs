﻿namespace _4.IS_principle_worker_edit.Contracts
{
    public interface ISleeper
    {
        void Sleep();
    }
}
